INSERT INTO `ke_menu` VALUES ('168', '评论管理', '68', '20', 'Comment/index', '0', '', '系统设置', '0', '1');
ALTER TABLE `ke_ad` ADD `type` int(1) NOT NULL DEFAULT '1' COMMENT '广告类型 1：图片 2：代码 3：文字';
ALTER TABLE `ke_document` ADD   `share_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分享次数';
ALTER TABLE `ke_goods` ADD   `share_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分享次数';
INSERT INTO `ke_attribute` VALUES ('36', 'share_count', '分享次数', 'int(11) UNSIGNED NOT NULL', 'num', '0', '', '1', '', '1', '0', '1', '1447743943', '1447743943', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `ke_config` VALUES ('72', 'WEB_SITE_NAME', '0', '网站名称', '0', '', '网站名称', '1447656587', '1447656587', '1', 'Ke361淘客商城', '0');
DROP TABLE IF EXISTS `ke_share`;
CREATE TABLE `ke_share` (
  `id` int(11) NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `count` int(11) unsigned NOT NULL DEFAULT '0',
  `object_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;